package pl.pmajewski.colorshooter2;

import android.os.Bundle;
import android.widget.Toast;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import pl.pmajewski.colorshooter2.ColorShooter;
import pl.pmajewski.colorshooter2.service.PlayService;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

public class AndroidLauncher extends AndroidApplication implements PlayService {
	
	private static final String AD_ID = "ca-app-pub-9911509688312197/5037401353";
	protected InterstitialAd interstitialAd;
	
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
		
		interstitialAd = new InterstitialAd(this);
		interstitialAd.setAdUnitId(AD_ID);
		interstitialAd.setAdListener(new AdListener() {
			@Override
			public void onAdLoaded() {
			}

			@Override
			public void onAdClosed() {
			}
		});

		AdRequest request = new AdRequest.Builder().build();
		interstitialAd.loadAd(request);
		
		config.numSamples = 2;
		initialize(new ColorShooter(this), config);
	}
	
	@Override
	public void loadAd() {
		try {
			runOnUiThread(new Runnable() {
				public void run() {
					// TODO TURN ON ADS
					AdRequest interstitialRequest = new AdRequest.Builder().build();
					interstitialAd.loadAd(interstitialRequest);
				}
			});
		} catch (Exception e) {
		}

	}

	@Override
	public void showAd() {
		try {
			runOnUiThread(new Runnable() {
				public void run() {
					//TURN ON ADS
					interstitialAd.show();
				}
			});
		} catch (Exception e) {
		}

	}
}
