package pl.pmajewski.colorshooter2.repository.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import pl.pmajewski.colorshooter2.model.EnemyModel;
import pl.pmajewski.colorshooter2.repository.EnemyRepository;

public class EnemyRepositoryImpl implements EnemyRepository {
	
	// Singleton instance
	private static EnemyRepositoryImpl instance;
	
	private List<EnemyModel> enemyList = new LinkedList<EnemyModel>();
	
	private EnemyRepositoryImpl() { }
	
	public static EnemyRepositoryImpl getInstance() {
		if(instance == null) {
			instance = new EnemyRepositoryImpl();
		}
		
		return instance;
	}

	@Override
	public List<EnemyModel> listAll() {
		//System.out.println("EnemyRepositoryImpl -> listAll() -> size: "+enemyList.size());
		return enemyList;
	}

	// TODO optymalizacja - ostatni mo�e by� trzymany jako referencja w repo, trzeba uwa�a� aby zmieni� referncje gdy dojdzie do usuni�cia danego model
	@Override
	public EnemyModel getLastAdded() {
		if(enemyList.size() == 1) {
			return enemyList.get(0);
		} else if(enemyList.size() == 0) {
			return null;
		} else {
			EnemyModel min = Collections.min(enemyList, new Comparator<EnemyModel>() {
				public int compare(EnemyModel o1, EnemyModel o2) {
					return o1.getLaunchTime() <= o2.getLaunchTime() ? 1 : -1;
				};
			});
			return min;
		}
	}

	@Override
	public void truncate() {
		enemyList = new ArrayList<EnemyModel>();
	}

	@Override
	public void add(EnemyModel enemy) {
		enemyList.add(enemy);
	}
}
