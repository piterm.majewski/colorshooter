package pl.pmajewski.colorshooter2.repository.impl;

import java.util.ArrayList;
import java.util.List;

import pl.pmajewski.colorshooter2.exceptions.EmptyRepositoryException;
import pl.pmajewski.colorshooter2.model.ShieldModel;
import pl.pmajewski.colorshooter2.repository.ShieldRepository;

public class ShieldRepositoryImpl implements ShieldRepository {
	
	// Singleton instance
	private static ShieldRepositoryImpl instance;
	
	// Repository attributes
	private List<ShieldModel> shieldList = new ArrayList<ShieldModel>();
	
	private ShieldRepositoryImpl() { }
	
	public static ShieldRepositoryImpl getInstance() {
		if(instance == null) {
			instance = new ShieldRepositoryImpl();
		}
		return instance;
	}
	
	@Override
	public ShieldModel get() {
		if(shieldList.isEmpty()) {
			throw new EmptyRepositoryException();
		}
		return shieldList.get(0);
	}

	@Override
	public void truncate() {
		shieldList = new ArrayList<ShieldModel>();
	}

	@Override
	public void add(ShieldModel shield) {
		shieldList.add(shield);
	}
}
