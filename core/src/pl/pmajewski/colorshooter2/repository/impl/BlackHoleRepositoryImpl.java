package pl.pmajewski.colorshooter2.repository.impl;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;

import pl.pmajewski.colorshooter2.exceptions.EmptyRepositoryException;
import pl.pmajewski.colorshooter2.exceptions.OneInstanceException;
import pl.pmajewski.colorshooter2.model.BlackHoleModel;
import pl.pmajewski.colorshooter2.repository.BlackHoleRepository;
import pl.pmajewski.colorshooter2.utils.GlobalProperties;
import pl.pmajewski.colorshooter2.utils.color.ColorManager;

public class BlackHoleRepositoryImpl implements BlackHoleRepository {
	
	// Singleton instance
	private static BlackHoleRepositoryImpl instance;
	// Repository attributes
	private List<BlackHoleModel> blackHoleList = new ArrayList<BlackHoleModel>();
	
	private BlackHoleRepositoryImpl() { }
	
	public static BlackHoleRepositoryImpl getInstance() {
		if(instance == null) {
			instance = new BlackHoleRepositoryImpl();
		}
		
		return instance;
	}
	
	@Override
	public void add(BlackHoleModel bhModel) {
		blackHoleList.add(bhModel);
	}
	
	@Override
	public BlackHoleModel get() {
		//System.out.println("BlackHoleRepository -> get() -> size: "+blackHoleList.size());
		if(blackHoleList.isEmpty()) {
			throw new EmptyRepositoryException();
		}
		return blackHoleList.get(0);
	}

	@Override
	public void truncate() {
		blackHoleList = new ArrayList<BlackHoleModel>();
	}
}
