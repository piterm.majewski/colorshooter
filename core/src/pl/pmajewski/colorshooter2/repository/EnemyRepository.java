package pl.pmajewski.colorshooter2.repository;

import java.util.List;

import pl.pmajewski.colorshooter2.model.EnemyModel;

public interface EnemyRepository {

	List<EnemyModel> listAll();

	EnemyModel getLastAdded();

	void truncate();

	void add(EnemyModel enemy);
}
