package pl.pmajewski.colorshooter2.repository;

import pl.pmajewski.colorshooter2.model.BlackHoleModel;

public interface BlackHoleRepository {
	
	BlackHoleModel get();
	void truncate();
	void add(BlackHoleModel bhModel);
}
