package pl.pmajewski.colorshooter2.memo;

import pl.pmajewski.colorshooter2.utils.GameState;

public class GameScreenMemo {
	
	private static GameScreenMemo instance;
	
	private int hits;
	private GameState gameState;
	private long lastEnemyLaunch;
	private long roundStart;
	private long gameTime;
	private long gameScreenLaunch;
	private long lastSpeedJump;
	private float speedScalar = 1;
	private long gameOverHit;
	private boolean adShowed = true;
	private long lastBackButtonPressed;
	
	private GameScreenMemo() { }
	
	public static GameScreenMemo getInstance() {
		if(instance == null) {
			instance = new GameScreenMemo();
		}
		
		return instance;
	}
	
	public int getHits() {
		return hits;
	}

	public void setHits(int hits) {
		this.hits = hits;
	}

	public void truncate() {
		instance = null;
	}

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	public long getLastEnemyLaunch() {
		return lastEnemyLaunch;
	}

	public void setLastEnemyLaunch(long lastEnemyLaunch) {
		this.lastEnemyLaunch = lastEnemyLaunch;
	}

	public void incHits() {
		hits++;
	}
	
	public void triggerRoundStart() {
		roundStart = System.currentTimeMillis();
	}
	
	public long getRoundStart() {
		return roundStart;
	}

	public long getGameScreenLaunch() {
		return gameScreenLaunch;
	}

	public void setGameScreenLaunch(long gameScreenLaunch) {
		this.gameScreenLaunch = gameScreenLaunch;
	}

	public void setRoundStart(long roundStart) {
		this.roundStart = roundStart;
	}

	public float getSpeedScalar() {
		return speedScalar;
	}

	public void setSpeedScalar(float speedScalar) {
		this.speedScalar = speedScalar;
	}

	public long getLastSpeedJump() {
		return lastSpeedJump;
	}

	public void setLastSpeedJump(long lastSpeedJump) {
		this.lastSpeedJump = lastSpeedJump;
	}

	public long getGameOverHit() {
		return gameOverHit;
	}

	public void setGameOverHit(long gameOverHit) {
		this.gameOverHit = gameOverHit;
	}

	public long getGameTime() {
		return gameTime;
	}

	public void setGameTime(long gameTime) {
		this.gameTime = gameTime;
	}

	public boolean isAdShowed() {
		return adShowed;
	}

	public void setAdShowed(boolean adShowed) {
		this.adShowed = adShowed;
	}

	public long getLastBackButtonPressed() {
		return lastBackButtonPressed;
	}

	public void setLastBackButtonPressed(long lastBackButtonPressed) {
		this.lastBackButtonPressed = lastBackButtonPressed;
	}
}
