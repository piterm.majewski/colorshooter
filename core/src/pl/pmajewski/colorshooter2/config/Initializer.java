package pl.pmajewski.colorshooter2.config;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Orientation;

import pl.pmajewski.colorshooter2.controller.MainMenuController;
import pl.pmajewski.colorshooter2.controller.PhysicsFunController;
import pl.pmajewski.colorshooter2.utils.GlobalProperties;

public class Initializer {
	
	public static void initializeGame() {
		// set launch screen - MainMenu
		initializeRotation();
		GlobalProperties.gameInstace.setScreen(new PhysicsFunController());
	}
	
	public static void initializeRotation() {
		int screenWidth = Gdx.graphics.getWidth();
		int screenHeight = Gdx.graphics.getHeight();
		Orientation nativeOrientation = Gdx.input.getNativeOrientation();
		
		if(screenWidth > screenHeight && nativeOrientation == Orientation.Portrait) {
			GlobalProperties.falseRotation = true;
			int temp = GlobalProperties.worldWidth;
			GlobalProperties.worldWidth = GlobalProperties.worldHeight;
			GlobalProperties.worldHeight = temp;
		}
	}
}
