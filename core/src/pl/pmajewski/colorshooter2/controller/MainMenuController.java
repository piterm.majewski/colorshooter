package pl.pmajewski.colorshooter2.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.FitViewport;

import pl.pmajewski.colorshooter2.component.AssetsManager;
import pl.pmajewski.colorshooter2.utils.GlobalProperties;

public class MainMenuController extends ScreenAdapter {

	// UIComponents
	private Stage stage;
	private ImageButton imgBtn;
	private AssetManager assetManager;
	
	public MainMenuController() {
		assetManager = new AssetsManager().getAssetManager();
		stage = new Stage(new FitViewport(GlobalProperties.worldWidth, GlobalProperties.worldHeight));
		
		// TODO te warto�ci powinny by� liczone dynamicznie tak aby przycisk zawsze znajdowa� si� na �rodku ekranu
		float buttonSize = 384;
		float imageWidth = 410;
		float imageHeight = 150;
		
		Image image = new Image(new TextureRegionDrawable(new TextureRegion(assetManager.get("ColorShooter.png", Texture.class))));
		image.setPosition(GlobalProperties.worldWidth/2-imageWidth/2, GlobalProperties.worldHeight/5*4-imageHeight/2);
		
		imgBtn = new ImageButton(new TextureRegionDrawable(new TextureRegion(assetManager.get("PlayButton.png", Texture.class))), 
				 new TextureRegionDrawable(new TextureRegion(assetManager.get("PlayButtonClicked.png", Texture.class))));
		imgBtn.setPosition(GlobalProperties.worldWidth/2-buttonSize/2, GlobalProperties.worldHeight/2-buttonSize/2);
		imgBtn.setSize(buttonSize, buttonSize);
		imgBtn.addListener(new ClickListener() {
			
			@Override
			public void clicked(InputEvent event, float x, float y) {
				//Gdx.app.log("", "Game should jump to GameScreen");
				//GlobalProperties.gameInstace.setScreen(ColorShooter.screens.get("gameScreen"));
				GlobalProperties.gameInstace.setScreen(new GameScreenController());
				dispose();
				super.clicked(event, x, y);
			}
		});
		
		stage.addActor(image);
		stage.addActor(imgBtn);
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1f/255f*33f, 1f/255f*33f, 1f/255f*33f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glEnable(GL20.GL_SAMPLES);
		stage.act();
		stage.draw();
	}
	
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void hide() {
		Gdx.input.setInputProcessor(null);
	}

	@Override
	public void dispose() {
		stage.dispose();
		assetManager.dispose();
	}
}
