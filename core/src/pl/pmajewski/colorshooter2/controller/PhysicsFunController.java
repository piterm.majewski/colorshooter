package pl.pmajewski.colorshooter2.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import pl.pmajewski.colorshooter2.utils.GlobalProperties;

public class PhysicsFunController extends ScreenAdapter {
	
	public static float PIX_BY_METER = 100;
	
	World world;
	Camera camera;
	Box2DDebugRenderer debugRender;
	Viewport viewport;
	Body body;
	Body floor;
	
	ShapeRenderer shapeRenderer;
	
	
	public PhysicsFunController() {
		world = new World(new Vector2(0, -9.8f), true);
		camera = new OrthographicCamera(c(GlobalProperties.worldWidth), c(GlobalProperties.worldHeight));
		camera.position.set(c(GlobalProperties.worldWidth/2), c(GlobalProperties.worldHeight/2), 0);
		viewport = new FillViewport(c(GlobalProperties.worldWidth), c(GlobalProperties.worldHeight), camera);
		viewport.apply();
		debugRender = new Box2DDebugRenderer(true, true, true, true, true, true);
	
		createDynamicCircle();
		createStaticFloor();
		
		// libgdx shape
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setProjectionMatrix(camera.combined);
	}
	
	private void createDynamicCircle() {
		// Box2d dynamic circle
		BodyDef bdef = new BodyDef();
		bdef.type = BodyType.DynamicBody;
		bdef.position.set(c(GlobalProperties.worldWidth/2), c(GlobalProperties.worldHeight));
		
		body = world.createBody(bdef);
		
		//CircleShape shape = new CircleShape();
		//shape.setRadius(c(25));
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(c(15), c(15));
		
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = 10f;
		fixtureDef.restitution = 0.1f;
		fixtureDef.friction = 0.3f;
		
		body.createFixture(fixtureDef);
		
		shape.dispose();
	}
	
	private void createStaticFloor() {
		// Box2d dynamic circle
		BodyDef bdef = new BodyDef();
		bdef.type = BodyType.KinematicBody;
		bdef.position.set(c(GlobalProperties.worldWidth/2), c(GlobalProperties.worldHeight/2));
		
		floor = world.createBody(bdef);
		
		PolygonShape shape = new PolygonShape();
		shape.setAsBox(c(GlobalProperties.worldWidth/2), c(25));
		
		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = shape;
		fixtureDef.density = 1f;
		fixtureDef.restitution = 0.3f;
		fixtureDef.friction = 2f;
		
		floor.createFixture(fixtureDef);
		shape.dispose();
	}
	
	private void drawSpot() {
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(Color.BLUE);
		shapeRenderer.circle(body.getPosition().x, body.getPosition().y, c(15), 360);
		shapeRenderer.identity();
		shapeRenderer.end();
		System.out.println("shapeRenderer -> x: "+body.getPosition().x+" y: "+body.getPosition().y);
	}
	
	private void update() {
		if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
			System.out.println("RightButton Clicked");
			body.setLinearVelocity(3, body.getLinearVelocity().y);
		}
		
		if(Gdx.input.isKeyPressed(Keys.LEFT)) {
			System.out.println("LeftButton Clicked");
			body.setLinearVelocity(-3, body.getLinearVelocity().y);
		}
		
		if(Gdx.input.isKeyPressed(Keys.UP)) {
			System.out.println("RightButton Clicked");
			body.setLinearVelocity(body.getLinearVelocity().x, 4.8f);
		}
		
		if(Gdx.input.isKeyPressed(Keys.DOWN)) {
			System.out.println("RightButton Clicked");
			body.setLinearVelocity(body.getLinearVelocity().x, -4.8f);
		}
	}
	
	@Override
	public void render(float delta) {
		update();
		world.step(delta, 1, 1);
		camera.position.set(body.getPosition().x, body.getPosition().y, 0);
		camera.update();
		Gdx.gl.glClearColor(0f, 0f, 0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		drawSpot();
		debugRender.render(world, camera.combined);
		System.out.println("camer.position.x: "+camera.position.x+" | body.position.x: "+body.getPosition().x);
		//System.out.println(body.getPosition());
	}
	
	public static float c(float value) {
		return value/PIX_BY_METER;
	}
	

}
