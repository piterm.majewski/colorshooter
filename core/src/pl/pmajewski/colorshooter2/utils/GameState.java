package pl.pmajewski.colorshooter2.utils;

public enum GameState {
	GAME, GAME_OVER, TEST;
}
