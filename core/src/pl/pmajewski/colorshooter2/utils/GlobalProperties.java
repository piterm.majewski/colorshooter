package pl.pmajewski.colorshooter2.utils;

import com.badlogic.gdx.Game;

import pl.pmajewski.colorshooter2.service.PlayService;

public class GlobalProperties {
	
	public static Game gameInstace;
	public static PlayService playService;
	
	public static int worldWidth=720;
	public static int worldHeight=1280;
	public static final float propabilitySameColor = 0.20f;
	public static final float propabilityAd = 0.40f;
	public static final float gameOverPause = 250; // in milis
	public static final float speedJump = 1000; // in milis
	
	// Game Characters Default Values
	public static final Float DEAULT_BLACK_HOLE_RADIUS = 120.0f;
	public static final Float DEFAULT_SHIELD_RADIUS = 180.0f;
	public static final Float DEFAULT_SHIELD_BOLD = 20.0f;
	public static final Float DEFAULT_ENEMY_RADIUS = 25.0f;

	public static boolean falseRotation = false;
}
