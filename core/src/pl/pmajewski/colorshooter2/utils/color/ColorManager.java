package pl.pmajewski.colorshooter2.utils.color;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.graphics.Color;

public class ColorManager {

	private static Map<Integer, ColorSchema> colors = new HashMap<Integer, ColorSchema>();
	private static ColorSchema selected;
	
	static {
		Schema1();
		randSchema();
	}
	
	private static void randSchema() {
		Double id = Math.random()*colors.size();
		selected = colors.get((Integer)id.intValue());	
	}
	
	private static void Schema1() {
		ColorSchema schema = new ColorSchema();
		
		Color bg = new Color();
		bg.set(1f/255f*33f, 1f/255f*33f, 1f/255f*33f, 1f);
		
		Color bh = new Color();
		bh.set(1f/255f*205f, 1f/255f*220f, 1f/255f*57f, 1f);
		
		Color sc = new Color();
		sc.set(1f/255f*255f, 1f/255f*255f, 1f/255f*255f, 1f);

		Color r1 = new Color();
		r1.set(1f/255f*230f, 1f/255f*95f, 1f/255f*92f, 1f);
		
		Color r2 = new Color();
		r2.set(1f/255f*204f, 1f/255f*0f, 1f/255f*204f, 1f); // pink
		
		Color r3 = new Color();
		r3.set(1f/255f*0f, 1f/255f*145f, 1f/255f*255f, 1f); // blue
		
		Color r4 = new Color();
		r4.set(1f/255f*165f, 1f/255f*209f, 1f/255f*243f, 1f);

		Color r5 = new Color();
		r5.set(1f/255f*173f, 1f/255f*35f, 1f/255f*94f, 1f);

		
		schema.setBgColor(bg);
		schema.setBhColor(bh);
		schema.setShieldColor(sc);
		schema.addRestColor(r1);
		schema.addRestColor(r2);
		schema.addRestColor(r3);
		schema.addRestColor(r4);
		schema.addRestColor(r5);
		
		colors.put(0, schema);
	}
	
	public static ColorSchema getSchema() {
		return selected;
	}
}
