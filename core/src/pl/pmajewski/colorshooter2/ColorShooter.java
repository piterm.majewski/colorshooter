package pl.pmajewski.colorshooter2;

import com.badlogic.gdx.Game;

import pl.pmajewski.colorshooter2.component.AssetsManager;
import pl.pmajewski.colorshooter2.config.Initializer;
import pl.pmajewski.colorshooter2.service.PlayService;
import pl.pmajewski.colorshooter2.utils.GlobalProperties;

public class ColorShooter extends Game {
	
	public ColorShooter(PlayService playService) {
		super();
		GlobalProperties.playService = playService;
	}

	@Override
	public void create () {
		GlobalProperties.gameInstace = this;
		Initializer.initializeGame();
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		super.dispose();
	}
}
