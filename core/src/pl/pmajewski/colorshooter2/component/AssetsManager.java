package pl.pmajewski.colorshooter2.component;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

/**
 * @author piter
 * Singleton
 */
public class AssetsManager {

	private AssetManager assetManager;
	
	public AssetsManager() { 
		this.assetManager = new AssetManager();
		
		assetManager.load("PlayButton.png", Texture.class);
		assetManager.load("PlayButtonClicked.png", Texture.class);
		assetManager.load("AgainButton.png", Texture.class);
		assetManager.load("AgainButtonClicked.png", Texture.class);
		assetManager.load("gameover.png", Texture.class);
		assetManager.load("ColorShooter.png", Texture.class);
		assetManager.finishLoading();
	}
	
	public AssetManager getAssetManager() {
		return assetManager;
	}

	public void setAssetManager(AssetManager assetManager) {
		this.assetManager = assetManager;
	}
}
