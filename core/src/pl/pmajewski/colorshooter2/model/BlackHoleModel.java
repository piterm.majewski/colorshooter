package pl.pmajewski.colorshooter2.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

public class BlackHoleModel extends GameModel {

	private Color color;
	private Vector2 position;
	private float radius;

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	@Override
	public void draw() {
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.identity();
		shapeRenderer.setColor(color);
		shapeRenderer.identity();
		shapeRenderer.circle(position.x, position.y, radius, 360);
		shapeRenderer.identity();
		shapeRenderer.end();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("BlackHoleModel | ");
		
		sb.append("color: ").append(color.toString()).append(" | ");
		sb.append("position: ").append(position.toString()).append(" | ");
		sb.append("radius: ").append(radius).append(" | ");
		
		return sb.toString();
	}
}
