package pl.pmajewski.colorshooter2.model;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public abstract class GameModel {
	
	protected ShapeRenderer shapeRenderer = new ShapeRenderer();
	
	public abstract void draw();

	public ShapeRenderer getShapeRenderer() {
		return shapeRenderer;
	}

	public void setShapeRenderer(ShapeRenderer shapeRenderer) {
		this.shapeRenderer = shapeRenderer;
	}
}
