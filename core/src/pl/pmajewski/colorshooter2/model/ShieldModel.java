package pl.pmajewski.colorshooter2.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

import pl.pmajewski.colorshooter2.utils.color.ColorManager;

public class ShieldModel extends GameModel {

	public enum DIRECTION {
		TOP, DOWN
	}

	private Color color;
	private Vector2 position;
	private float radius;
	private float arcStart;
	private float shieldBold;
	private DIRECTION direction;

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public float getArcStart() {
		return arcStart;
	}

	public void setArcStart(float arcStart) {
		this.arcStart = arcStart;
	}

	public float getShieldBold() {
		return shieldBold;
	}

	public void setShieldBold(float shieldBold) {
		this.shieldBold = shieldBold;
	}

	public DIRECTION getDirection() {
		return direction;
	}

	public void setDirection(DIRECTION direction) {
		this.direction = direction;
	}

	@Override
	public void draw() {
		//System.out.println("ShieldModel -> draw()");
		//System.out.println("  |--shield: "+this.toString());
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(color);
		shapeRenderer.arc(position.x, position.y, radius, arcStart, 180, 180);
		shapeRenderer.setColor(ColorManager.getSchema().getBgColor());
		shapeRenderer.arc(position.x, position.y, radius-shieldBold, arcStart, 180, 180);
		shapeRenderer.end();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("ShieldModel | ");
		
		sb.append("color: ").append(color).append(" | ");
		sb.append("position: ").append(position).append(" | ");
		sb.append("radius: ").append(radius).append(" | ");
		sb.append("ardStart: ").append(arcStart).append(" | ");
		sb.append("shieldBold: ").append(shieldBold).append(" | ");
		sb.append("direction: ").append(direction).append(" | ");
		
		return sb.toString();
	}
}
