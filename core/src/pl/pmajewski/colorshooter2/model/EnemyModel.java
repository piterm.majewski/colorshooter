package pl.pmajewski.colorshooter2.model;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

public class EnemyModel extends GameModel {

	public enum LaunchPosition {
		TOP, DOWN
	}

	private float speed;
	private float speedScalar;
	private Color color;
	private Vector2 position;
	private Vector2 velocity;
	private float radius;
	private float launchTime;
	private LaunchPosition launchPosition;

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public float getSpeedScalar() {
		return speedScalar;
	}

	public void setSpeedScalar(float speedScalar) {
		this.speedScalar = speedScalar;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Vector2 getPosition() {
		return position;
	}

	public void setPosition(Vector2 position) {
		this.position = position;
	}

	public Vector2 getVelocity() {
		return velocity;
	}

	public void setVelocity(Vector2 velocity) {
		this.velocity = velocity;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public float getLaunchTime() {
		return launchTime;
	}

	public void setLaunchTime(float launchTime) {
		this.launchTime = launchTime;
	}

	public LaunchPosition getLaunchPosition() {
		return launchPosition;
	}

	public void setLaunchPosition(LaunchPosition launchPosition) {
		this.launchPosition = launchPosition;
	}

	@Override
	public void draw() {
		//System.out.println("EnemyModel -> draw()");
		//System.out.println("  |--enemy: "+this.toString());
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(color);
		shapeRenderer.circle(position.x, position.y, radius, 360);
		shapeRenderer.end();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("EnemyModel | ");
		
		sb.append("position: ").append(position).append(" | ");
		sb.append("speed: ").append(speed).append(" | ");
		sb.append("speedScalar: ").append(speedScalar).append(" | ");
		sb.append("color: ").append(color).append(" | ");
		sb.append("velocity").append(velocity).append(" | ");
		sb.append("radius").append(radius).append(" | ");
		sb.append("launchTime: ").append(launchTime).append(" | ");
		sb.append("launchPosition").append(launchPosition).append(" | ");
		
		return sb.toString();
	}
}
