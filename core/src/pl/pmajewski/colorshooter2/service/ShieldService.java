package pl.pmajewski.colorshooter2.service;

import com.badlogic.gdx.graphics.OrthographicCamera;

import pl.pmajewski.colorshooter2.model.ShieldModel;

public interface ShieldService {
	
	void generate(OrthographicCamera camera);
	void rotate();
	void truncate();
	ShieldModel get();
}
