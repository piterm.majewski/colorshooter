package pl.pmajewski.colorshooter2.service;

import com.badlogic.gdx.graphics.OrthographicCamera;

import pl.pmajewski.colorshooter2.model.BlackHoleModel;

public interface BlackHoleService {
	
	void generate(OrthographicCamera camera);
	
	void truncate();
	
	BlackHoleModel get();
}
