package pl.pmajewski.colorshooter2.service.impl;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

import pl.pmajewski.colorshooter2.model.ShieldModel;
import pl.pmajewski.colorshooter2.model.ShieldModel.DIRECTION;
import pl.pmajewski.colorshooter2.repository.ShieldRepository;
import pl.pmajewski.colorshooter2.repository.impl.ShieldRepositoryImpl;
import pl.pmajewski.colorshooter2.service.ShieldService;
import pl.pmajewski.colorshooter2.utils.GlobalProperties;
import pl.pmajewski.colorshooter2.utils.color.ColorManager;

public class ShieldServiceImpl implements ShieldService {

	// Business logic fields
	private ShieldRepository shieldRepository = ShieldRepositoryImpl.getInstance();
	
	public ShieldServiceImpl() { }

	@Override
	public void generate(OrthographicCamera camera) {
		ShieldModel defaultShield = getDefaultShield();
		defaultShield.getShapeRenderer().setProjectionMatrix(camera.combined);
		shieldRepository.add(defaultShield);
	}
	
	private ShieldModel getDefaultShield() {
		Vector2 position = new Vector2();
		position.x = GlobalProperties.worldWidth/2;
		position.y = GlobalProperties.worldHeight/2;

		ShieldModel sh = new ShieldModel();
		sh.setColor(ColorManager.getSchema().getShieldColor());
		sh.setPosition(position);
		sh.setRadius(GlobalProperties.DEFAULT_SHIELD_RADIUS);
		sh.setShieldBold(GlobalProperties.DEFAULT_SHIELD_BOLD);
		sh.setArcStart(0);
		sh.setDirection(DIRECTION.TOP);

		
		return sh;
	}
	
	@Override
	public void rotate() {
		ShieldModel shield = shieldRepository.get();
		
		if (shield.getArcStart() == 180) {
			 shield.setArcStart(0);
			 shield.setDirection(DIRECTION.TOP);
		} else {
			shield.setArcStart(180);
			shield.setDirection(DIRECTION.DOWN);
		}
	}

	@Override
	public void truncate() {
		shieldRepository.truncate();
	}

	@Override
	public ShieldModel get() {
		return shieldRepository.get();
	}
}
