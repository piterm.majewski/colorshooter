package pl.pmajewski.colorshooter2.service.impl;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

import pl.pmajewski.colorshooter2.model.BlackHoleModel;
import pl.pmajewski.colorshooter2.repository.BlackHoleRepository;
import pl.pmajewski.colorshooter2.repository.impl.BlackHoleRepositoryImpl;
import pl.pmajewski.colorshooter2.service.BlackHoleService;
import pl.pmajewski.colorshooter2.utils.GlobalProperties;
import pl.pmajewski.colorshooter2.utils.color.ColorManager;

public class BlackHoleServiceImpl implements BlackHoleService {
	
	// Business logic fields
	private BlackHoleRepository bhRepo = BlackHoleRepositoryImpl.getInstance();
	
	// Singleton methods
	public BlackHoleServiceImpl() { }
	
	// Business logic methods
	
	@Override
	public void generate(OrthographicCamera camera) {
		BlackHoleModel defaultBlackHole = getDefaultBlackHole();
		defaultBlackHole.getShapeRenderer().setProjectionMatrix(camera.combined);
		bhRepo.add(defaultBlackHole);
	}
	
	private BlackHoleModel getDefaultBlackHole() {
		BlackHoleModel bh = new BlackHoleModel();
	
		Vector2 v2 = new Vector2();
		v2.x = GlobalProperties.worldWidth/2;
		v2.y = GlobalProperties.worldHeight/2;
		
		bh.setPosition(v2);
		bh.setRadius(GlobalProperties.DEAULT_BLACK_HOLE_RADIUS);
		bh.setColor(ColorManager.getSchema().getBhColor());
		
		return bh;
	}

	@Override
	public void truncate() {
		bhRepo.truncate();
	}

	@Override
	public BlackHoleModel get() {
		return bhRepo.get();
	}
}
