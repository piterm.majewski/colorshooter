package pl.pmajewski.colorshooter2.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import pl.pmajewski.colorshooter2.ColorShooter;
import pl.pmajewski.colorshooter2.service.PlayService;

public class DesktopLauncher {

	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 450;
		config.height = 800;
		config.samples = 8;
		config.allowSoftwareMode=true;
		config.foregroundFPS = 60;
		PlayService ps = new PlayService() {
			
			@Override
			public void loadAd() {
				System.out.println("PlayService -> loadAdd()");
			}

			@Override
			public void showAd() {
				System.out.println("PlayService -> showAdd()");
			}
		};
		new LwjglApplication(new ColorShooter(ps), config);
	}
}
